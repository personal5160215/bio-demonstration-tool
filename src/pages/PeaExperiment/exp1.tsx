import { useEffect, useState, useRef, useCallback, useMemo } from "react"
import { Button, H3, Menu, MenuItem, Popover, Switch } from "@blueprintjs/core"
import Title from "../../components/Title"

type Choice = {
	name: "A" | "a",
	weight: number,
}

type Generator = {
	name: string,
	choices: [
		Choice,
		Choice,
	]
}

const useRandomPicker = (choices: Choice[])=> {
	const SIZE = 1000
	const cache = useRef<Choice["name"][]>([])
	const currentIndex = useRef(0)

	useEffect(()=> {
		// generate result by weight and shiffle it
		const result = choices.flatMap(({name, weight})=> Array(weight * SIZE).fill(name))
		for (let i = result.length - 1; i > 0; i--) {
			const j = Math.floor(Math.random() * (i + 1));
			[result[i], result[j]] = [result[j], result[i]];
		}
		cache.current = result
		currentIndex.current = 0
	}, [choices])
	
	return ()=> {
		if (currentIndex.current >= cache.current.length) currentIndex.current = 0
		return cache.current[currentIndex.current++]
	}
}

const SubTitle = (props: {children: React.ReactNode, className?: string})=> (
	<H3 style={{marginTop: 10}} className={props.className}>{props.children}</H3>
)

export default function LawOfSegregation(props: {maxResults?: number}) {
  const { maxResults = 400 } = props
	const [result, setResult] = useState<{id: number, picked: ("A" | "a")[]}[]>([])
	const [sort, setSort] = useState<"id" | "p1" | "p2">("id")
	const [enablePicker2, setEnablePicker2] = useState(false)

	const gen1: Generator = {
		name: "父本",
		choices: [
			{name: "A", weight: 1},
			{name: "a", weight: 1},
		]
	}

	const gen2: Generator = {
		...gen1,
		name: "母本",
	}

	const listRef = useRef<HTMLDivElement>(null)
	const canInsert = result.length < maxResults
	const picker1 = useRandomPicker(gen1.choices)
	const picker2 = useRandomPicker(gen2.choices)
	const insertResult = useCallback(()=> {
		if (!canInsert) return // TODO: fix the bug that count actually may out of limit
		const p1 = picker1()
		const p2 = picker2()
		setResult(list=> ([...list,
			{id: list.length + 1, picked: [p1, p2]},
		]))
		requestAnimationFrame(()=> listRef.current!.scrollTop = 999999)
	}, [canInsert, picker1, picker2])

	const formatGenerator = useCallback((geno: Generator) => {
		return geno.choices.map(({name})=> name).join("")
	}, [])

	const delay = useRef<number>(Infinity)

	useEffect(()=> {
		let timer = 0
		const insert = ()=> {
			if (delay.current >= 0) 
				delay.current -= 1
			else if (canInsert)
				insertResult()
			else
				delay.current = Infinity
	
			timer = requestAnimationFrame(insert)
		}
		insert()
		return ()=> cancelAnimationFrame(timer)
	}, [insertResult, canInsert])

	const onMouseDown = useCallback(()=> {
		insertResult()
		delay.current = 20
	}, [insertResult])

	const onMouseUp = useCallback(()=> {
		delay.current = Infinity
	}, [])

	const onMouseLeave = onMouseUp

	const formatCount = useCallback((count: number)=> {
		const total = result.length
		if (total === 0) return (<strong className="text-lg indent-5">/</strong>)
		else return <strong className="text-lg indent-5">{`${count} / ${total} (${(count/total*100).toFixed(1)}%)`}</strong>
	}, [result])

	const sortedResult = useMemo(()=> {
		return result.toSorted((a, b)=> {
			if (sort === "id") return a.id - b.id
			if (sort === "p1") return -a.picked[0].localeCompare(b.picked[0])
			if (sort === "p2") return -a.picked[1].localeCompare(b.picked[1])
			return 0
		})
	}, [result, sort])

	const GameteDisplay2 = useCallback((props: GameteDisplayProps)=> {
		if (enablePicker2) return <GameteDisplay {...props}/>
	}, [enablePicker2])

  return (
		<>
			<div>
				<Title title="分离定律" subtitle="Law of Segregation" align="center"/>
				<div className="flex">
					<div className="bp5-elevation-4 p-2 rounded-sm" style={{width: 400}}>
						<div className="flex flex-grow">
							<div className="w-full h-20 m-2 p-2 bp5-elevation-1 relative">
								<SubTitle>{gen1.name} <i>{formatGenerator(gen1)}</i></SubTitle>
							</div>
							<div className="w-full h-20 m-2 p-2 bp5-elevation-1 relative">
								<SubTitle className={!enablePicker2 ? "opacity-20" : ""}>{gen2.name} <i>{formatGenerator(gen2)}</i></SubTitle>
								<div className="absolute right-2 top-2">
									<Popover minimal content={
										<div className="p-2">
											<Switch label="开启" checked={enablePicker2} onChange={()=> setEnablePicker2(v=> {
												setResult([]) // clear all results when switching female picker on/off
												return !v
											})}/>
										</div>
									}>
										<Button icon="cog"/>
									</Popover>
								</div>
							</div>
						</div>
						<div className="bp5-elevation-1 m-2 p-2 relative">
							<SubTitle>
								配子
							</SubTitle>
							<div className="absolute right-2 top-2">
								<Button icon="play" className="mr-2" 
									onMouseDown={onMouseDown}
									onMouseUp={onMouseUp}
									onMouseLeave={onMouseLeave}
									disabled={!canInsert}
								/>
								<Button icon="trash" className="mr-2" onClick={()=> setResult([])}/>
								<Popover minimal content={
										<Menu>
											<MenuItem text="默认排序" onClick={()=> setSort("id")}/>
											<MenuItem text="按父本排序" onClick={()=> setSort("p1")}/>
											<MenuItem text="按母本排序" onClick={()=> setSort("p2")}/>
										</Menu>
									}>
									<Button icon="sort"/>
								</Popover>
							</div>
							{/* result list */}
							<div ref={listRef} style={{height: "calc(100vh - 200px)", overflowY: "auto", overscrollBehaviorY: "none"}}>
								{
									sortedResult.map(({id, picked})=> 
										<div key={id} className="flex w-full p-2 pl-0 text-xl items-center">
											<div className="mr-2 text-slate-400 w-12 text-right shrink-0">({id})</div>
											<div className="w-full"><GameteDisplay name={picked[0]}/></div>
											<div style={{width: 20}}/>
											<div className="w-full">
												{enablePicker2 && <GameteDisplay name={picked[1]}/>}
											</div>
										</div>)
								}
							</div>
						</div>
					</div>
					<div style={{width: 20}}/>
					<div className="bp5-elevation-4 p-4 rounded-sm" style={{width: 500}}>
						<div className="bp5-elevation-1 p-2 mb-4">
							<SubTitle>{gen1.name}</SubTitle>
							{
								gen1.choices.map(({name})=> (
									<div className="flex items-center">
										<GameteDisplay margin={5} width={100} key={name} name={name}/>
										<GameteDisplay2 margin={5} width={100} key={"_"} name={"_"}/>
										{
											formatCount(result.filter(({picked})=> picked[0] === name).length)
										}
									</div>
								))
							}
							{
								enablePicker2 && (
									<>
										<SubTitle>{gen2.name}</SubTitle>
										{
											gen2.choices.map(({name})=> (
												<div className="flex items-center">
													<GameteDisplay margin={5} width={100} key={"_"} name={"_"}/>
													<GameteDisplay2 margin={5} width={100} key={name} name={name}/>
													{
														formatCount(result.filter(({picked})=> picked[1] === name).length)
													}
												</div>
											))
										}
									</>
								)
							}
						</div>
						<div className="bp5-elevation-1 p-2 mb-4" style={{display: enablePicker2 ? "block": "none"}}>
							<SubTitle>排列</SubTitle>
							{
								["AA", "Aa", "aA", "aa"].map((name)=> (
									<div className="flex items-center">
										<GameteDisplay margin={5} width={100} key={name} name={name[0]}/>
										<GameteDisplay2 margin={5} width={100} key={name} name={name[1]}/>
										{
											formatCount(result.filter(({picked})=> picked.join("") === name).length)
										}
									</div>
								))
							}
							<SubTitle>组合（基因型）</SubTitle>
							{
								["AA", "Aa", "aa"].map((name)=> (
									<div className="flex items-center">
										<GameteDisplay margin={5} width={100} key={name} name={name[0]}/>
										<GameteDisplay margin={5} width={100} key={name} name={name[1]}/>
										{
											formatCount(result.filter(({picked})=> picked.toSorted().join("") === name).length)
										}
									</div>
								))
							}
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

type GameteDisplayProps = {
	name: "A" | "a" | "_" | (string & {}),
	width?: number,
	height?: number,
	margin?: number,
}

function GameteDisplay(props: GameteDisplayProps) {
	const { name } = props
	const { width, height, margin } = props
	const borderColor = name === "A" ? "red" : "#999"
	const backgroundColor = name === "A" ? "#f003" : name === "a" ? "#0001" : "#0000"
	const borderStyle = name === "_" ? "dotted" : "solid"
	return (
		<div style={{
			border: `2px ${borderStyle} ${borderColor}`, 
			borderRadius: 2,
			backgroundColor,
			padding: 4,
			paddingLeft: 10,
			width, height, margin,
		}}>
			<div className="w-full"><strong><i>{name}</i></strong></div>
		</div>
	)
}
