import { useEffect, useState, useRef, useCallback, useMemo } from "react"
import { Button, H3, Menu, MenuItem, Popover, Switch } from "@blueprintjs/core"
import Title from "../../components/Title"

type Choice = {
	name: "AA" | "Aa" | "aa" | "BB" | "Bb" | "bb",
	weight: number,
}

type Generator = {
	name: string,
	choices: [
		Choice,
		Choice,
		Choice,
	]
}

const useRandomPicker = (choices: Choice[])=> {
	const SIZE = 1000
	const cache = useRef<Choice["name"][]>([])
	const currentIndex = useRef(0)

	useEffect(()=> {
		// generate result by weight and shiffle it
		const result = choices.flatMap(({name, weight})=> Array(weight * SIZE).fill(name))
		for (let i = result.length - 1; i > 0; i--) {
			const j = Math.floor(Math.random() * (i + 1));
			[result[i], result[j]] = [result[j], result[i]];
		}
		cache.current = result
		currentIndex.current = 0
	}, [choices])
	
	return ()=> {
		if (currentIndex.current >= cache.current.length) currentIndex.current = 0
		return cache.current[currentIndex.current++]
	}
}

const SubTitle = (props: {children: React.ReactNode, style?: React.CSSProperties, className?: string, onClick?: ()=> void})=> (
	<H3 
		style={{marginTop: 10, ...props.style}} 
		className={props.className}
		onClick={props.onClick}>
		{props.children}
	</H3>
)

export default function LawOfIndependentAssortment(props: {maxResults?: number}) {
  const { maxResults = 800 } = props
	const [result, setResult] = useState<{id: number, picked: Choice["name"][]}[]>([])
	const [sort, setSort] = useState<"id" | "p1" | "p2">("id")
	const [enablePicker2, setEnablePicker2] = useState(false)

	const gen1: Generator = {
		name: "等位基因",
		choices: [
			{name: "AA", weight: 1},
			{name: "Aa", weight: 2},
			{name: "aa", weight: 1},
		]
	}

	const gen2: Generator = {
		name: "等位基因",
		choices: [
			{name: "BB", weight: 1},
			{name: "Bb", weight: 2},
			{name: "bb", weight: 1},
		]
	}

	const listRef = useRef<HTMLDivElement>(null)
	const canInsert = result.length < maxResults
	const picker1 = useRandomPicker(gen1.choices)
	const picker2 = useRandomPicker(gen2.choices)
	const insertResult = useCallback(()=> {
		if (!canInsert) return // TODO: fix the bug that count actually may out of limit
		const p1 = picker1()
		const p2 = picker2()
		setResult(list=> ([...list,
			{id: list.length + 1, picked: [p1, p2]},
		]))
		requestAnimationFrame(()=> listRef.current!.scrollTop = 999999)
	}, [canInsert, picker1, picker2])

	const formatGenerator = useCallback((geno: Generator) => {
		const name = geno.choices[1].name
		return `${name.charAt(0)}/${name.charAt(1)}`
	}, [])

	const delay = useRef<number>(Infinity)

	useEffect(()=> {
		let timer = 0
		const insert = ()=> {
			if (delay.current >= 0) 
				delay.current -= 1
			else if (canInsert)
				[insertResult(), insertResult()]
			else
				delay.current = Infinity
	
			timer = requestAnimationFrame(insert)
		}
		insert()
		return ()=> cancelAnimationFrame(timer)
	}, [insertResult, canInsert])

	const onMouseDown = useCallback(()=> {
		insertResult()
		delay.current = 20
	}, [insertResult])

	const onMouseUp = useCallback(()=> {
		delay.current = Infinity
	}, [])

	const onMouseLeave = onMouseUp

	const formatCount = useCallback((count: number)=> {
		const total = result.length
		if (total === 0) return (<strong className="text-lg indent-5">/</strong>)
		else return <strong className="text-lg indent-5">{`${count} / ${total} (${(count/total*100).toFixed(1)}%)`}</strong>
	}, [result])

	const sortedResult = useMemo(()=> {
		return result.toSorted((a, b)=> {
			if (sort === "id") return a.id - b.id
			if (sort === "p1") return -a.picked[0].localeCompare(b.picked[0])
			if (sort === "p2") return -a.picked[1].localeCompare(b.picked[1])
			return 0
		})
	}, [result, sort])

	const GameteDisplay2 = useCallback((props: GameteDisplayProps)=> {
		if (enablePicker2) return <GameteDisplay {...props}/>
	}, [enablePicker2])

	const [combine, setCombine] = useState<"genotype" | "phenotype">("genotype")

  return (
		<>
			<div>
				<Title title="自由组合定律" subtitle="Law of Independent Assortment" align="center"/>
				<div className="flex">
					<div className="bp5-elevation-4 p-2 rounded-sm" style={{width: 500}}>
						<div className="flex flex-grow">
							<div className="w-full h-20 m-2 p-2 bp5-elevation-1 relative">
								<SubTitle>{gen1.name} <i>{formatGenerator(gen1)}</i></SubTitle>
							</div>
							<div className="w-full h-20 m-2 p-2 bp5-elevation-1 relative">
								<SubTitle className={!enablePicker2 ? "opacity-20" : ""}>{gen2.name} <i>{formatGenerator(gen2)}</i></SubTitle>
								<div className="absolute right-2 top-2">
									<Popover minimal content={
										<div className="p-2">
											<Switch label="开启" checked={enablePicker2} onChange={()=> setEnablePicker2(v=> {
												setResult([]) // clear all results when switching female picker on/off
												return !v
											})}/>
										</div>
									}>
										<Button icon="cog"/>
									</Popover>
								</div>
							</div>
						</div>
						<div className="bp5-elevation-1 m-2 p-2 relative">
							<SubTitle>
								基因型
							</SubTitle>
							<div className="absolute right-2 top-2">
								<Button icon="play" className="mr-2" 
									onMouseDown={onMouseDown}
									onMouseUp={onMouseUp}
									onMouseLeave={onMouseLeave}
									disabled={!canInsert}
								/>
								<Button icon="trash" className="mr-2" onClick={()=> setResult([])}/>
								<Popover minimal content={
										<Menu>
											<MenuItem text="默认排序" onClick={()=> setSort("id")}/>
											<MenuItem text="按A/a基因排序" onClick={()=> setSort("p1")}/>
											<MenuItem text="按B/b基因排序" onClick={()=> setSort("p2")}/>
										</Menu>
									}>
									<Button icon="sort"/>
								</Popover>
							</div>
							{/* result list */}
							<div ref={listRef} style={{height: "calc(100vh - 200px)", overflowY: "auto", overscrollBehaviorY: "none"}}>
								{
									sortedResult.map(({id, picked})=> 
										<div key={id} className="flex w-full p-2 pl-0 text-xl items-center">
											<div className="mr-2 text-slate-400 w-12 text-right shrink-0">({id})</div>
											<div className="w-full"><GameteDisplay name={picked[0]}/></div>
											<div style={{width: 20}}/>
											<div className="w-full">
												{enablePicker2 && <GameteDisplay name={picked[1]}/>}
											</div>
										</div>)
								}
							</div>
						</div>
					</div>
					<div style={{width: 20}}/>
					<div className="bp5-elevation-4 p-4 rounded-sm" style={{width: 500}}>
						<div className="bp5-elevation-1 p-2 mb-4">
							<SubTitle>{gen1.name}</SubTitle>
							{
								gen1.choices.map(({name})=> (
									<div className="flex items-center">
										<GameteDisplay margin={5} width={100} key={name} name={name}/>
										<GameteDisplay2 margin={5} width={100} key={"__"} name={"__"}/>
										{
											formatCount(result.filter(({picked})=> picked[0] === name).length)
										}
									</div>
								))
							}
							{
								enablePicker2 && (
									<>
										{/* <SubTitle>{gen2.name}</SubTitle> */}
										<hr/>
										{
											gen2.choices.map(({name})=> (
												<div className="flex items-center">
													<GameteDisplay margin={5} width={100} key={"__"} name={"__"}/>
													<GameteDisplay2 margin={5} width={100} key={name} name={name}/>
													{
														formatCount(result.filter(({picked})=> picked[1] === name).length)
													}
												</div>
											))
										}
									</>
								)
							}
						</div>
						<div className="bp5-elevation-1 p-2 mb-4" style={{display: enablePicker2 ? "block": "none"}}>
							<div className="flex">
								<SubTitle className="cursor-pointer" style={{color: combine !== "genotype" ? "#ccc" : undefined}}
									onClick={()=> setCombine("genotype")}>组合（基因型）</SubTitle>
								<div className="w-px bg-slate-200 mx-4 h-10"/>
								<SubTitle className="cursor-pointer" style={{color: combine === "genotype" ? "#ccc" : undefined}}
									onClick={()=> setCombine("phenotype")}>组合（表现型）</SubTitle>
							</div>
							{/* eslint-disable */}
							{
								combine === "genotype" &&
								["AABB", "AaBB", "aaBB",
								 "AABb", "AaBb", "aaBb",
								 "AAbb", "Aabb", "aabb"].map((name)=> (
									<div className="flex items-center" key={name}>
										<GameteDisplay margin={5} width={100} name={name.substring(0, 2)}/>
										<GameteDisplay2 margin={5} width={100} name={name.substring(2, 4)}/>
										{
											formatCount(result.filter(({picked})=> picked.join("") === name).length)
										}
									</div>
								))
							}
							{
								combine === "phenotype" &&
								["A_B_", "A_bb", "aaB_", "aabb"].map((name)=> (
									<div className="flex items-center" key={name}>
										<GameteDisplay margin={5} width={100} name={name.substring(0, 2)}/>
										<GameteDisplay2 margin={5} width={100} name={name.substring(2, 4)}/>
										{
											formatCount(result.filter(({picked})=> {
												const full = picked[0].replace("AA", "A_").replace("Aa", "A_")
												           + picked[1].replace("BB", "B_").replace("Bb", "B_")
												return full === name}).length)
										}
									</div>
								))
							}
							{/* eslint-enable */}
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

type GameteDisplayProps = {
	name: Choice["name"] | "__" | (string & {}),
	width?: number,
	height?: number,
	margin?: number,
}

function GameteDisplay(props: GameteDisplayProps) {
	const { name } = props
	const { width, height, margin } = props
	const hasA = name.charCodeAt(0) === "A".charCodeAt(0)
	const hasB = name.charCodeAt(0) === "B".charCodeAt(0)
	const hasU = name.startsWith("_")
	const borderColor = hasA ? "red" : hasB ? "blue" : "#999"
	const backgroundColor = hasA ? "#f003" : hasB ? "#00f3" : !hasU ? "#0001" : "#0000"
	const borderStyle = hasU ? "dotted" : "solid"
	return (
		<div style={{
			border: `2px ${borderStyle} ${borderColor}`, 
			borderRadius: 2,
			backgroundColor,
			padding: 4,
			paddingLeft: 10,
			width, height, margin,
		}}>
			<div className="w-full"><strong><i>{name}</i></strong></div>
		</div>
	)
}
