import LawOfSegregation from './exp1'
import LawOfIndependentAssortment from './exp2'

const PeaExperiment = {
	LawOfSegregation,
	LawOfIndependentAssortment,
}

export default PeaExperiment