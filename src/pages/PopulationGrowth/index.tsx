// j / s curve growth display
// TODO: 是否需要手动拖拽调整斜率和截距？
import { useRef, useEffect, useMemo, useState, useCallback } from 'react'
import * as echarts from 'echarts/core'
import { SVGRenderer } from 'echarts/renderers'
import { LineChart } from 'echarts/charts'
import { UniversalTransition } from 'echarts/features'
import { DataZoomComponent, GridComponent, TitleComponent, TooltipComponent } from 'echarts/components'
import { Button, Card, Dialog, DialogBody, H2 } from '@blueprintjs/core'
import './style.css'
import { useNavigate, useSearchParams } from 'react-router-dom'

echarts.use([SVGRenderer, GridComponent, TitleComponent, LineChart, UniversalTransition, DataZoomComponent, TooltipComponent])

const birthRate = { name: "出生率", color: "red", }
const deathRate = { name: "死亡率", color: "blue", }
const birthNum = { name: "出生数", color: "red", }
const deathNum = { name: "死亡数", color: "blue", }
const increaseNum = { name: "净增长数", color: "black", }
const population = { name: "个体数", color: "black", }

const XAXIS = {
  NUMBER: "数量",
  TIME: "时间",
}

type Param = [number, number] // start value, slope 

function useFigData(param: Param) {
  const [startValue, slope] = param
  return {
    fig1: (i: number)=> startValue + slope * i,
    fig2: (i: number)=> startValue * i + slope * i * i,
  }
}

export default function PopulationGrowth() {
  const [param1, setParam1] = useState<Param>([1, -0.00])
  const [param2, setParam2] = useState<Param>([0.0, 0.00])
  // TODO: this state is changable in future
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [n0, setN0] = useState(4) // for decreasing curve (eg. death > birth), this value should be set to 1000
  const [showConfig, setShowConfig] = useState(false)

  const modelName = useSearchParams()[0].get("model") || "1"
  const fixedModelName = ["1", "2", "3"].includes(modelName) ? modelName : "1"
  useEffect(()=> {
    switch (fixedModelName) {
      case "1": setParam1([1, 0.00]); setParam2([0, 0.00]); break
      case "2": setParam1([1, 0.00]); setParam2([0.5, 0.00]); break
      case "3": setParam1([1, -0.001]); setParam2([0.5, 0.001]); break
    }
  }, [fixedModelName])

  const data1 = useFigData(param1)
  const data2 = useFigData(param2)
  const fig3 = useCallback((i: number)=> data1.fig2(i) - data2.fig2(i), [data1, data2])
  const [fig4, platformTime] = useMemo(()=> {
    const cache = [n0]
    for (let i = 0; i < 1000; i++) {
      const last = cache[cache.length - 1]
      const inc = fig3(last)* 0.1 // make it increase slower
      cache.push(last + inc)
      if (Math.abs(inc) < 0.001) break
      if (last > 100000) break
    }
    return [
      cache,
      cache.length,
    ]
  }, [n0, fig3])

  const intersectX = (param1[0] - param2[0]) / (param2[1] - param1[1])
  const maxX = intersectX < 10000 ? intersectX * 1.2 : 100

  useEffect(()=> {
    const onKeyDown = (e: KeyboardEvent)=> {
      if (e.key === "m") setShowConfig(true)
    }
    window.addEventListener("keydown", onKeyDown)
    return ()=> window.removeEventListener("keydown", onKeyDown)
  }, [])

  useEffect(()=> {
    let showTooltip = true
    const onKeyDown = (e: KeyboardEvent)=> {
      if (e.key === "t") {
        showTooltip = !showTooltip
        document.getElementsByTagName("body")[0].classList.toggle("hide-echart-tooltip", !showTooltip)
      }
    }
    window.addEventListener("keydown", onKeyDown)
    return ()=> window.removeEventListener("keydown", onKeyDown)
  }, [])

  const navigate = useNavigate()

  return (
    <div className="relative">
      <div className="flex flex-wrap p-2 bg-slate-400 select-none" style={{width: "100vw", height: "100vh"}}>
        {/* 2x2 grid */}
        <div className="w-1/2 h-1/2 p-2 relative">
          <div className="bp5-elevation-1 w-full h-full rounded-sm bg-white">
            <Chart title={`${birthRate.name}/${deathRate.name}`}
              xAxisLabel={XAXIS.NUMBER}
              maxX={maxX}
              curves={[
                {...birthRate, fn: data1.fig1},
                {...deathRate, fn: data2.fig1},
              ]}
            />
          </div>
          <div className="absolute top-4 right-4">
            <Button icon="cog" large onClick={()=> setShowConfig(true)}></Button>
          </div>
        </div>
        <div className="w-1/2 h-1/2 p-2">
          <div className="bp5-elevation-1 w-full h-full rounded-sm bg-white">
            <Chart title={`${birthNum.name}/${deathNum.name}`}
              xAxisLabel={XAXIS.NUMBER}
              maxX={maxX}
              curves={[
                {...birthNum, fn: data1.fig2},
                {...deathNum, fn: data2.fig2},
              ]}
            />
          </div>
        </div>
        <div className="w-1/2 h-1/2 p-2">
          <div className="bp5-elevation-1 w-full h-full rounded-sm bg-white">
            <Chart title={increaseNum.name}
              xAxisLabel={XAXIS.NUMBER}
              maxX={maxX}
              curves={[
                {...increaseNum, fn: fig3},
              ]}
            />
          </div>
        </div>
        <div className="w-1/2 h-1/2 p-2">
          <div className="bp5-elevation-1 w-full h-full rounded-sm bg-white">
            <Chart title={population.name}
              xAxisLabel={XAXIS.TIME}
              maxX={platformTime}
              curves={[
                {...population, data: fig4.map((v, i)=> [i, v])},
              ]}
            />
          </div>
        </div>
      </div>
      <div className="absolute top-2 right-2">
      </div>
      <Dialog 
        title="切换模型（快捷键M）"
        isOpen={showConfig} 
        onClose={()=> setShowConfig(false)}
        backdropProps={{style: {backgroundColor: "#fffa"}}}>
        <DialogBody>
          <div className="p-0">
          {
            [
              ["模型1", "出生率固定/死亡率为0", [1, 0.00], [0, 0.00]],
              ["模型2", "出生率固定/死亡率固定", [1, 0.00], [0.5, 0.00]],
              ["模型3", "出生率和死亡率随数量变化", [1, -0.001], [0.5, 0.001]],
            ].map(([title, desc], i)=> (
              <Card key={i} interactive className="p-2 m-2 rounded-sm bg-white"
                onClick={()=> {
                  // setParam1(param1 as Param)
                  // setParam2(param2 as Param)
                  navigate(`/population-growth?model=${i + 1}`)
                  setShowConfig(false)
                }}>
                <H2 className="text-xl">{title}</H2>
                <p className="text-lg text-slate-400">{desc}</p>
              </Card>)
            )
          }
          </div>
        </DialogBody>
      </Dialog>
    </div>
  )
}

type Curve = {
  name: string,
  color: string,
  fn?: (x: number)=> number,
  data?: [number, number][],
}

type ChartProps = {
  title: string,
  xAxisLabel?: string,
  maxX: number,
  curves: Curve[],
  axisLabelFontSize?: number,
}

function Chart(props: ChartProps) {
  const container = useRef<HTMLDivElement>(null)
  const { axisLabelFontSize = 25 } = props
  const { title, curves, xAxisLabel, maxX } = props
  const axisConfig = useMemo(()=> ({ 
    axisLabel: { fontSize: axisLabelFontSize },
    type: "value",
    axisLine: { onZero: false, lineStyle: { color: "#224", width: 4, cap: "square" } },
    axisTick: { lineStyle: { color: "#224", width: 4, cap: "square" } },
    minorTick: { show: false },
    min: 0,
  }), [axisLabelFontSize])

  useEffect(()=> {
    const chart = echarts.init(container.current!)
    chart.setOption({
      title: {
        text: title,
        textStyle: { fontSize: 30 }
      },
      grid: {
        left: 110,
        right: 90,
        // top: 50,
        bottom: 80,
      },
      xAxis: {
        name: xAxisLabel,
        nameTextStyle: { fontSize: axisLabelFontSize },
        ...axisConfig,
      },
      yAxis: {
        name: "",
        ...axisConfig,
      },
      animation: false,
      series: curves.map(({name, color, fn, data})=> ({
        name,
        type: 'line',
        showSymbol: false,
        data: data || Array.from({length: maxX + 1}, (_, i)=> [i, fn!(i)]),
        smooth: true,
        color,
        lineStyle: { width: 8 },
        tooltip: { 
          show: true, 
          valueFormatter: (v: number)=> v.toFixed(v < 100 ? 2 : v < 1000 ? 1 : 0)
        },
      })),
      dataZoom: [
        {
          type: 'slider',
          xAxisIndex: 0,
          show: true,
          filterMode: 'filter'
        },
        // {
        //   type: 'slider',
        //   yAxisIndex: 0,
        //   show: true,
        //   filterMode: 'empty'
        // },
        {
          type: "inside",
          xAxisIndex: 0,
        },
        {
          type: "inside",
          yAxisIndex: 0,
        },
      ],
      tooltip: {
        trigger: 'axis',
        textStyle: { fontSize: axisLabelFontSize, color: "#333"},
        axisPointer: { type: 'cross' },
        className: "echart-tooltip",
      },
    })
    // @ts-ignore link chart to node
    // container.current!.chart = chart
    return ()=> {
      chart.dispose()
    }
  }, [title, curves, axisConfig, xAxisLabel, maxX, axisLabelFontSize])

  return (
    <div ref={container} className="w-full h-full">

    </div>
  )
}
