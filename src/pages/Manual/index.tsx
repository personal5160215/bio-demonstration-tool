import { useCallback } from "react"
import { Button, H2, IconName, UL } from "@blueprintjs/core"
// preview images
import pea1 from "./imgs/pea1.jpg"
import pea2 from "./imgs/pea2.jpg"
import chi from "./imgs/chi.jpg"
import pop from "./imgs/pop.jpg"

export default function Manual() {
  const imgClass = "w-10/12 rounded-md mx-auto my-2"
  const InlineButton = useCallback((props: {icon: IconName})=> {
    return <Button small icon={props.icon} className="mx-1"/>
  }, [])
  return (
    <div className="bp5-running-text">
      <H2>分离定律</H2>
      <UL>
        <li>点击第一个按钮<InlineButton icon="play"/>，随机生成一组配子；长按可快速生成，模拟次数上限为400。</li>
        <li>点击第二个按钮<InlineButton icon="trash"/>，清空模拟结果。</li>
        <li>点击第三个按钮<InlineButton icon="sort"/>，对结果进行排序。</li>
        <li>点击母本右上角的齿轮<InlineButton icon="cog"/>，启用或禁用母本配子（注意：切换后自动清空所有模拟结果）。</li>
        <li>右侧为统计数据。</li>
        <img className={imgClass} src={pea1} />
      </UL>
      <H2>自由组合定律</H2>
      <UL>
        <li>用法和分离定律演示工具类似。</li>
        <li>统计结果可在基因型（9种）和表型（4种）之间切换。</li>
        <img className={imgClass} src={pea2} />
      </UL>
      <H2>种群增长曲线</H2>
      <UL>
        <li>四张图分别是出生率/死亡率随种群数变化、出生数/死亡数随种群数变化、净增长数随种群数变化、种群数随<b>时间变化。</b></li>
        <li>在图表上滚动鼠标可进行缩放，也可拖动x轴下方的浅蓝色条实现缩放。</li>
        <li>点击左上角图的齿轮<InlineButton icon="cog"/>或按快捷键M切换模型，模型1和模型2为J型增长，模型3为S型增长。</li>
        <li>按快捷键T切换是否显示数据浮层。</li>
        <img className={imgClass} src={pop} />
      </UL>
      <H2>卡方检验</H2>
      <UL>
        <li>用于检查一个比例到底是不是3:1。</li>
        <li>样本总数指被统计的豌豆数量，预期比例是遗传模型假设的比例，默认3:1，实际比例是在实验中统计得到的比例。</li>
        <li>修改实际比例，会发现P值变化，且在超出某个范围后，拒绝假设。</li>
        <li>修改样本总数也会影响P值。</li>
        <img className={imgClass.replace("w-10/12", "w-1/2")} src={chi} />
      </UL>
    </div>
  )
}