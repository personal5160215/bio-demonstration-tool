import { Button, Card, Checkbox, H3, InputGroup, InputGroupProps, Tooltip } from "@blueprintjs/core"
import Title from "../../components/Title"
import { useMemo, useState } from "react"
// @ts-ignore type declaration file is unnecessary
import Analyse from 'chi-square-p-value'

export default function ChiSquareTesting() {
  return (
    <div>
      <Title title="卡方检验" subtitle="Chi-square Testing" />
      <Test1/>
    </div>
  )
}

function formatNumber(num: number, numDigits?: number) {
  if (isNaN(num)) return "NaN"
  return num.toFixed(typeof numDigits === "number" ? numDigits : 4)
}

function Test1() {
  const [total, setTotal] = useState<number>(1000)
  const [expWeights, setExpWeights] = useState<[number, number]>([3, 1])
  const [gotWeights, setGotWeights] = useState<[number, number]>([3.14, 1])
  const [isInt, setIsInt] = useState<boolean>(true)
  const isInputValid = !isNaN(total) && !expWeights.some(isNaN) && !gotWeights.some(isNaN)

  const testResult = useMemo(()=> {
    if (!isInputValid) return
    const totalExpWeight = expWeights.reduce((a, b)=> a + b, 0)
    const totalGotWeight = gotWeights.reduce((a, b)=> a + b, 0)
    const expValues = expWeights.map(w=> w / totalExpWeight * total).map(isInt ? Math.round : v=> v)
    const gotValues = gotWeights.map(w=> w / totalGotWeight * total).map(isInt ? Math.round : v=> v)
    const result = Analyse([expValues, gotValues])
    return {...result, expValues, gotValues}
  }, [expWeights, gotWeights, isInputValid, total, isInt])

  return (
    <Card>
      <H3 style={{marginTop: 20}}>总数-比例</H3>
      <p className="text-lg" style={{marginTop: 15}}>样本总数</p>
      <div className="flex items-center">
      <InputGroup 
        large
        className="m-1"
        type="number"
        value={total as any} 
        onChange={e=> setTotal(Number(e.currentTarget.value))}
      />
      <Checkbox label="是否取整"
        className="m-2"
        checked={isInt}
        onChange={e=> setIsInt(e.currentTarget.checked)}
      />
      </div>
      <p className="text-lg" style={{marginTop: 15}}>预期比例</p>
      <div className="flex text-lg items-center">
        <FloatInputGroup className="w-30 m-1" value={expWeights[0] as any} onChangeNumber={v=> setExpWeights(w=> [v, w[1]])}/>
        /
        <FloatInputGroup className="w-30 m-1" value={expWeights[1] as any} onChangeNumber={v=> setExpWeights(w=> [w[0], v])}/>
      </div>
      <p className="text-lg" style={{marginTop: 15}}>实际比例</p>
      <div className="flex text-lg items-center">
        <FloatInputGroup className="w-30 m-1" value={gotWeights[0] as any} onChangeNumber={v=> setGotWeights(w=> [v, w[1]])}/>
        /
        <FloatInputGroup className="w-30 m-1" value={gotWeights[1] as any} onChangeNumber={v=> setGotWeights(w=> [w[0], v])}/>
      </div>
      <hr/>
      <p className="text-lg">计算结果</p>
      {
        isInputValid ? <div>
          <p>预期：{testResult.expValues.map((v: number)=> formatNumber(v, isInt ? 0 : 1)).join(" / ")}</p>
          <p>实际：{testResult.gotValues.map((v: number)=> formatNumber(v, isInt ? 0 : 1)).join(" / ")}</p>
          <p>χ<sup>2</sup>值： {formatNumber(testResult.chi[0])}</p>
          <p>P值：{testResult.pValue}
            <Tooltip content={
              <span>
                在统计学中，以<code>P值 {"<"} 0.05</code>作为拒绝假设的标准。
              </span>
            }>
              <Button 
                minimal 
                className="m-1"
                icon={testResult.pValue > 0.05 ? "tick-circle" : "cross-circle"} 
                intent={testResult.pValue > 0.05 ? "success" : "danger"}
              />
            </Tooltip>
          </p>
        </div> : <p>/</p>}
    </Card>
  )
}

function FloatInputGroup(props: InputGroupProps & {onChangeNumber: (value: number)=> void}) {
  const [value, setValue] = useState<string>(props.value!.toString())
  const [isValid, setIsValid] = useState<boolean>(true)
  const setValueImpl = (value: string)=> {
    setValue(value)
    const num = parseFloat(value)
    props.onChangeNumber?.(isNaN(num) ? NaN : num)
    setIsValid(!isNaN(num))
  }

  return <InputGroup {...props}
    large
    autoComplete="off"
    spellCheck={false}
    type="text"
    value={value}
    intent={isValid ? "none" : "danger"}
    onChange={(e)=> setValueImpl(e.target.value)}
  />
}