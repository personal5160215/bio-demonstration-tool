import { H1 } from '@blueprintjs/core'

type TitleProps = {
  /** main title, usually in Chinese */
  title: string,
  /** sub title, usually in English */
  subtitle?: string,
  align?: "left" | "center" | "right",
}

export default function Title(props: TitleProps) {
  const { title, subtitle } = props
  const { align = "center"} = props
  const justify = align === "center" ? "justify-center" : align === "left" ? "justify-start" : "justify-end"
  return (
    <div className={"flex smiley-sans " + justify}>
      <H1>
        {title}
      </H1>
      {
        subtitle && (
          <H1 className="pl-2" style={{color: "#999"}}>
            {subtitle}
          </H1>
        )
      }
    </div>
  )
}
