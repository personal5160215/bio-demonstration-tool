import { Callout, Dialog, DialogBody, H1 } from "@blueprintjs/core"
import { useNavigate } from "react-router"
import Title from "../Title"
import { useState } from "react"
import Manual from "../../pages/Manual"

export default function ToolList() {
  const navigate = useNavigate()
  const [isOpen, setIsOpen] = useState(false)
  return (
    <div className="mt-5">
      <Title title="演示工具"/>
      <div className="sm:block md:hidden w-80 py-px">
        <Callout intent="warning">
          <p>建议在电脑上使用，手机会有一部分显示不全。</p>
        </Callout>
      </div>
      <div className="bp5-elevation-2 w-80 py-px">
        {
          [
            ["分离定律", "/pea-experiment-1"],
            ["自由组合定律", "/pea-experiment-2"],
            ["种群增长曲线", "/population-growth"],
            ["卡方检验（简易版）", "/chi-square-test"],
            ["使用指南", "#"],
          ].map(([title, url]) => (
            <div className="m-2" key={url}>
              <div 
                className="p-2 text-lg cursor-pointer hover:bg-slate-100 rounded-sm"
                onClick={()=> url === "#" ?
                  setIsOpen(true) : navigate(url)}
              >
                {title}
              </div>
            </div>
          ))
        }
      </div>
      <Dialog 
        title=""
        style={{width: 800}}
        isOpen={isOpen} onClose={()=>setIsOpen(false)}>
        <DialogBody>
          <H1>使用指南</H1>
          <Manual/>
        </DialogBody>
      </Dialog>
    </div>
  )
}