import React from 'react'
import { FallbackProps } from 'react-error-boundary'

export default function ErrorDisplay(props: FallbackProps) {
  const {error} = props
  return (
    <div className="p-4 text-lg">
      <p>发生了一些问题：</p>
      <pre className="text-red-500">{error.message}</pre>
      <div className="mt-8"></div>
      {/* eslint-disable-next-line */}
      <a onClick={()=> location.pathname = location.pathname}>刷新</a>
      <a className="ml-3" href="/">回到首页</a>
    </div>
  )
}