import './App.css'
import { FocusStyleManager } from '@blueprintjs/core'
import Routes from './routes'
import './polyfill'
FocusStyleManager.onlyShowFocusOnTabs()

function App() {
  return (
    <div className="bp5-running-text">
      <div className="flex w-full justify-center">
        <Routes/>          
      </div>
    </div>
  )
}

// reference:
// https://www.khanacademy.org/science/ap-biology/cellular-energetics/cellular-energy/a/atp-and-reaction-coupling

export default App
