import { Route, Routes } from "react-router"
import PeaExperiment from "./pages/PeaExperiment"
import PopulationGrowth from "./pages/PopulationGrowth"
import ToolList from "./components/ToolList"
import ChiSquareTesting from "./pages/ChiSquareTesting"
export default function AppRoutes() {
  return (
    <Routes>
      <Route path="/" element={<ToolList/>} />
      <Route path="/population-growth" element={<PopulationGrowth/>} />
      <Route path="/pea-experiment-1"  element={<PeaExperiment.LawOfSegregation/>} />
      <Route path="/pea-experiment-2"  element={<PeaExperiment.LawOfIndependentAssortment/>} />
      <Route path="/chi-square-test"   element={<ChiSquareTesting/>} />

      <Route path="*" element={<NotFound/>} />
    </Routes>
  )
}

function NotFound() {
  return (
    <div className="mx-auto mt-4 text-center text-lg">
      <h1>404 Not found</h1>
      <p>无效的网址，<a href="/" className="mx-px">点我</a>返回首页。</p>
    </div>
  )
}