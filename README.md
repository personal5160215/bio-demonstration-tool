# 生物学演示工具

一个网页版的生物学概念演示和模拟工具，包含:

- 分离定律
- 自由组合定律
- 种群增长曲线
- 卡方检测

网站: [biodemo.lwdocs.cn](biodemo.lwdocs.cn)，如果你和你的学生需要这个，请自由使用。

## 本地部署

```
npm i
npm run dev
```